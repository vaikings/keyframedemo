//
//  ViewController.m
//  KeyframeDemo
//
//  Created by Vaibhav Bhatia on 10/30/13.
//  Copyright (c) 2013 Vaikings. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UISwitch *autoReverseSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *fillModeSwitch;
@property (weak, nonatomic) IBOutlet UISlider *offsetSlider;

@property (strong, nonatomic) CABasicAnimation* colorAnimation ;

@property (weak, nonatomic) IBOutlet UIStepper *durationStepper;
@property (weak, nonatomic) IBOutlet UIStepper *beginTimeStepper;
@property (weak, nonatomic) IBOutlet UIStepper *repeatCountStepper;
@property (weak, nonatomic) IBOutlet UIStepper *repeatDurationStepper;
@property (weak, nonatomic) IBOutlet UIStepper *speedStepper;


- (IBAction)resetButtonTapped:(UIButton *)sender;

- (IBAction)timeOffsetUpdated:(UISlider *)sender;
- (IBAction)autoReverseTapped:(UISwitch*)sender;
- (IBAction)fillModeTapped:(UISwitch *)sender;
- (IBAction)durationStepperTapped:(UIStepper*)sender;
- (IBAction)beginTimeTapped:(UIStepper *)sender;
- (IBAction)repeatCountTapped:(UIStepper *)sender;
- (IBAction)repeatDurationTapped:(UIStepper *)sender;
- (IBAction)speedTapped:(UIStepper *)sender;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.colorAnimation  = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    self.colorAnimation.fromValue = (id)[UIColor orangeColor].CGColor;
    self.colorAnimation.toValue   = (id)[UIColor blueColor].CGColor;
    
    self.colorAnimation.duration  = 1.0; // For convenience
    self.durationStepper.minimumValue = 1.0  ;
    self.durationStepper.maximumValue = 3.0 ;
    self.durationStepper.stepValue = 0.5 ;
    
    [self.colorView.layer addAnimation:self.colorAnimation
                        forKey:@"ChangeColor"];
    
    self.colorView.layer.speed = 0.0; // Pause the animation
    self.colorView.layer.fillMode = kCAFillModeRemoved ;
    
    self.autoReverseSwitch.on = NO ;
    self.colorAnimation.autoreverses = NO ;
    
    self.fillModeSwitch.on = NO ;
    self.colorAnimation.fillMode = kCAFillModeRemoved ;

    self.colorAnimation.beginTime = 0.0 ;
    self.beginTimeStepper.minimumValue = 0.0 ;
    self.beginTimeStepper.maximumValue = 2.0 ;
    
    self.colorAnimation.repeatCount = 1.0 ;
    self.repeatCountStepper.minimumValue = 1.0 ;
    self.repeatCountStepper.maximumValue = 3.0 ;

    self.colorAnimation.repeatDuration = 0.0 ;
    self.repeatDurationStepper.minimumValue = 0.0 ;
    self.repeatDurationStepper.maximumValue = 2.0 ;
    
    self.speedStepper.minimumValue = 1.0 ;
    self.speedStepper.maximumValue = 2.0 ;
    self.speedStepper.stepValue = 0.5 ;
    self.colorAnimation.speed = 1.0 ;
}

- (IBAction)resetButtonTapped:(UIButton *)sender {
    self.colorAnimation.duration  = 1.0 ;
    
    self.colorView.layer.speed = 0.0 ;
    self.speedStepper.value = self.speedStepper.minimumValue ;
    self.colorView.layer.timeOffset = 0.0 ;
    self.offsetSlider.value = 0.0 ;
    
    self.colorAnimation.autoreverses = NO ;
    self.colorAnimation.fillMode = kCAFillModeRemoved ;
    
    self.durationStepper.value = self.durationStepper.minimumValue ;
    self.colorAnimation.beginTime = 0.0 ;
    self.beginTimeStepper.value = self.beginTimeStepper.minimumValue ;
    self.colorAnimation.repeatCount = 1.0 ;
    self.repeatCountStepper.value = self.repeatCountStepper.minimumValue ;
    self.colorAnimation.repeatDuration = 0.0 ;
    self.repeatDurationStepper.value = self.repeatDurationStepper.minimumValue ;
    self.colorAnimation.speed = 1.0 ;
    self.speedStepper.value = self.speedStepper.minimumValue ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
    
    self.valueLabel.text = @"";
}

- (IBAction)timeOffsetUpdated:(UISlider *)sender {
    self.colorView.layer.timeOffset = sender.value ;
    self.valueLabel.text = [NSString stringWithFormat:@"Time offset = %0.2f", sender.value] ;
}

- (IBAction)autoReverseTapped:(UISwitch*)sender {
    self.colorAnimation.autoreverses = sender.isOn ;
    
    self.valueLabel.text = [NSString stringWithFormat:@"AutoReverse is %@", sender.isOn?@"YES":@"NO"] ;
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)fillModeTapped:(UISwitch *)sender {
    if (sender.isOn){
        self.colorAnimation.fillMode = kCAFillModeForwards ;
    }else{
        self.colorAnimation.fillMode = kCAFillModeBackwards ;
    }
    
    self.valueLabel.text = [NSString stringWithFormat:@"fillmode is %@", sender.isOn?@"Forward":@"Backward"] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)durationStepperTapped:(UIStepper*)sender {
    double stepperValue = sender.value ;
    self.colorAnimation.duration = stepperValue ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
    
    self.valueLabel.text = [NSString stringWithFormat:@"Duration = %0.2f", sender.value] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)beginTimeTapped:(UIStepper *)sender {
    double stepperValue = sender.value ;
    self.colorAnimation.beginTime = stepperValue ;
    
    self.valueLabel.text = [NSString stringWithFormat:@"Begin Time = %0.2f", sender.value] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)repeatCountTapped:(UIStepper *)sender {
    double stepperValue = sender.value ;
    self.colorAnimation.repeatCount = stepperValue ;
    
    self.valueLabel.text = [NSString stringWithFormat:@"Repeat Count = %0.2f", sender.value] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)repeatDurationTapped:(UIStepper *)sender {
    double stepperValue = sender.value ;
    self.colorAnimation.repeatDuration = stepperValue ;
    
    self.valueLabel.text = [NSString stringWithFormat:@"Repeat Duration = %0.2f", sender.value] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}

- (IBAction)speedTapped:(UIStepper *)sender {
    double stepperValue = sender.value ;
    self.colorAnimation.speed = stepperValue ;
    
    self.valueLabel.text = [NSString stringWithFormat:@"speed = %0.2f", sender.value] ;
    
    [self.colorView.layer removeAnimationForKey:@"ChangeColor"];
    [self.colorView.layer addAnimation:self.colorAnimation forKey:@"ChangeColor"];
}



@end
